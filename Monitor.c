#include <dirent.h> 
#include <stdio.h> 
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#define BUFFER_SIZE 40

unsigned int sleep(unsigned int seconds);
//This variable determines which directory the compile & run functions should search to find a program
char * jobsDirectory = "./jobs/";

//This method takes a program name in a character array and translates it into an array that will compile said program upon system command
void compileString(char *name){
    //First we define the prefix 'gcc' which we are using to compile the target C program
    char * gcc = "gcc ";
	char * dest = "-o"
    //Here we declare a character array that is large enough to hold the entire compile-ready array
    char cString[80];
	char destName[50];
	int truncateLen = strlen(name) - 2;
	strncpy(destName, name, (sizeof(name) * 2));
	destName[truncateLen] = '\0';
    /*
    In order to combine all of these arrays, we use the functions of strncpy & strncat.
    These functions require null terminating characters at the end of each array.
    In order to determine where we should put these characters, we use the strlen function on any array that is a variable.
    */
	int dDirectoryLength = strlen(jobsDirectory) + 6;
	int dNameLength = strlen(name) + dDirectoryLength;
    int sDirectoryLength = strlen(jobsDirectory) + dNameLength;
    //Each length must include the length of that before it
    int sNameLength = strlen(name) + sDirectoryLength;
    //Now we assign the null terminating characters
    cString[4] = '\0';
	cString[6] = '\0';
	cString[dNameLength] = '\0';
    cString[dDirectoryLength] = '\0';
    cString[sNameLength] = '\0';
	cString[sDirectoryLength] = '\0';
    //Here we begin construncting our final string
    strncpy(cString, gcc, sizeof(gcc));
	strncat(cString, dest, sizeof(dest));
	strncat(cString, jobsDirectory, sizeof(jobsDirectory));
	strncat(cString, destName, (sizeof(name) * 2));
    strncat(cString, jobsDirectory, sizeof(jobsDirectory));
    strncat(cString, name, (sizeof(name) * 2));
    system(cString);
}

/*
This method is the same as the compiling method, except with far less concatenations
We are essentially just adding the directory path in front of the program name and then passing it to the system
*/
void runString(char *rName){
    char rS[80];
	char trim[50];
	int truncateLen = strlen(rName) - 2;
	strncpy(trim, rName, (sizeof(rName) * 2));
	trim[truncateLen] = '\0';
    int directoryLength = strlen(jobsDirectory);
    rS[directoryLength] = '\0';
    strncpy(rS, jobsDirectory, sizeof(jobsDirectory));
    strncat(rS, trim, (sizeof(trim) * 2));
    system(rS);
}

//Ths method actually opens a directory and prints out its contents
int displayDirectory(char selectedDir[12]){
    DIR *d;
    char * point;
    struct dirent *dir;
    d = opendir(selectedDir);
    if (d) {
      while ((dir = readdir(d)) != NULL) {
        //This conditional sorts out unconventional files
        if(dir->d_type == DT_REG){
           //This conditional removes all file names with an extension to prevent double printing
           if((point = strrchr(dir->d_name, '.')) == NULL){
              printf("%s\n",dir->d_name);
           }
        }
      }
      closedir(d);
    }
    sleep(1);
    return(0);
}

//This method compiles and runs every program in the globally specified program directory
int runDirectory(char selectedDir[12]){
    DIR *d;
    char * point;
    struct dirent *dir;
    //We open the directory similar to how we would if we were just displaying the names inside
    d = opendir(selectedDir);
      if (d) {
         while ((dir = readdir(d)) != NULL) {
           if(dir->d_type == DT_REG){
              if((point = strrchr(dir->d_name, '.')) == NULL){
                 //After sorting the file, we simply put the name into our previous compile & run methods for each loop(or file)
                 compileString(dir->d_name);
                 runString(dir->d_name);
              }
           }
         }
      }
    closedir(d);
    sleep(1);
    return(0);
}

int listOptions(void){
   int input;
   printf("--------------------------------------------------------\n");
   printf("1. List Jobs\n");
   printf("2. Set Jobs Directory\n");
   printf("3. Compile and Run Specific Program\n");
   printf("4. Compile and Run All Jobs In The Set Directory\n");
   printf("5. List Program Options\n");
   printf("6. Help\n");
   printf("7. Shutdown\n");
   printf("--------------------------------------------------------\n");
   scanf("%d", &input);
   printf("\n");
   if ((input != 1) && (input != 2) && (input != 3) && (input != 4) && (input != 5) && (input != 6) && (input != 7)){
      printf("No Such Command\n");
      listOptions();
   }
   else{
     if (input == 7){
         return(0);
     }
     if (input == 1){
        sleep(1);
        displayDirectory("./jobs");
        listOptions();
     }
     if (input == 2){
        char directoryName[20];
        printf("Enter A Directory");
        scanf("%s", directoryName);
        jobsDirectory = directoryName;
        printf("%s\n", jobsDirectory);
        listOptions();
     }
     if (input == 3){
        char programName[20];
        printf("Enter Program Path\n");
        scanf("%s", programName);
        printf("\n");
        compileString(programName);
        runString(programName);
        sleep(1);
        printf("\n");
        listOptions();
     }
     if (input == 4){
        sleep(1);
        runDirectory("./jobs");
        listOptions();
     }
     if (input == 5){
        printf("1.  Delete A Program");
        printf("2.  Move A Program");
     }
     if ( input == 6){
        printf("We don't know?\n");
        printf("Just google it\n");

        sleep(1);
        listOptions();
     }
   }
}

int main(void) {
  printf("--------------------------------------------------------\n");
  printf("                     W E L C O M E                      \n");
  listOptions();
}
