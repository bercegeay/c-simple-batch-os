
#include <stdio.h>

void factorial(int, int);

void main()
{
    int counter = 1;
    int result = 1;
    factorial(counter, result);
}

void factorial(int counter, int result)
{

     if(counter == 11)
     {
          return;
     }
     else
     {
          result = result * counter;
          counter++;
          printf("%d    \n", result);
          factorial(counter, result);
     }
}

