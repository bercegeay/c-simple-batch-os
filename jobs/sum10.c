
#include <stdio.h>

void display(int);

void main()
{
     int current = 1;
     display(current);
}

void display(int num)
{
     static int i = 2;

     if(i == 11)
     {
        printf("%d   \n", num);
        return;
     }
     else
     {
        printf("%d   \n", num);
        num = i + num;
        i++;
        display(num);
     }
}

